%%%-------------------------------------------------------------------
%% @doc planetone public API
%% @end
%%%-------------------------------------------------------------------

-module(planetone_app).

-behaviour(application).

-define(CHANN_NAME,  channel1).

-export([start/2, stop/1, start_client_channel/0]).

start(_StartType, _StartArgs) ->
    planetone_sup:start_link().

stop(_State) ->
    grpc_client_sup:stop_channel_pool(?CHANN_NAME),
    ok.

%%--------------------------------------------------------------------
%% APIs

start_client_channel() ->
    ClientOps = #{},
    SvrAddr = "http://172.18.0.2:9080",
    {ok, _} = grpc_client_sup:create_channel_pool(
                ?CHANN_NAME,
                SvrAddr,
                ClientOps
               ),
    logger:info("Started client channel ~s for ~s successfully!~n~n",
              [?CHANN_NAME, SvrAddr]).



%% internal functions
