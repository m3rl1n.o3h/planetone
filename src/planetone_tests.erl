-module(planetone_tests).
-include_lib("eunit/include/eunit.hrl").

-define(GRPC_OK, [{<<"grpc-status">>, <<"0">>}, {<<"grpc-message">>, <<>>}]).
-define(ALTER_OK, #{'Data' => <<>>}).

dgraph_client_test_() ->
    {"Run basic transaction tests for the Dgraph server",
        {setup, fun init_client/0, fun term_client/1, fun txn_query_withvars/1}}.

dgraph_check_mutations_test_() ->
    {"Run basic mutation tests for the Dgraph server",
     {setup, fun init_client/0, fun term_client/1, fun txn_query_mutate/1}}.

dgraph_check_bytes_test_() ->
    {"Run basic bytes input tests for the Dgraph server",
     {setup, fun init_client/0, fun term_client/1, fun txn_mutate_bytes/1}}.

dgraph_query_unmarshel_test_() ->
    {"Run basic response from query unmarshling test for the Dgraph server",
     {setup, fun init_client/0, fun term_client/1, fun txn_query_unmarshal/1}}.

txn_query_withvars(Channel) ->
    [
        ?_assertEqual(
           {ok, ?ALTER_OK, ?GRPC_OK}, api_dgraph_client:alter(#{drop_all => 1}, Channel)
        ),
        ?_assertEqual(
            {ok, ?ALTER_OK, ?GRPC_OK},
            api_dgraph_client:alter(
                #{schema => "name: string @index(exact) .\n type Person {\n name \n }"}, Channel
            )
        ),
        ?_assertMatch(
            {ok, _, ?GRPC_OK},
            api_dgraph_client:query(
                #{
                    mutations => [
                        #{
                            set_json => jsone:encode(#{
                                name => <<"Alice">>, 'dgraph.type' => [<<"Person">>]
                            })
                        }
                    ],
                    commit_now => true
                },
                Channel
            )
        ),
     ?_assertMatch(
        {ok, _, ?GRPC_OK},
        api_dgraph_client:query(
          #{
            query =>
                "query Alice($a: string){ me(func: eq(name, $a)) { name	dgraph.type }	}",
            vars => #{"$a" => "Alice"}
           },
          Channel
         )
       )
    ].

txn_query_mutate(Channel) ->
    Person = #{
               uid => <<"_:alice">>,
               name => <<"Alice">>,
               age => 26,
               married => true,
               'dgraph.type' => [<<"Person">>],
               loc => #{type => <<"Point">>, coordinates => [1.1, 2]},
               raw_bytes => [21, 32, 34, 11, 13],
               friends => [
                           #{name => <<"Bob">>, age => 24, 'dgraph.type' => [<<"Person">>]},
                           #{name => <<"Charlie">>, age => 29, 'dgraph.type' => [<<"Person">>]}
                          ],
               school => #{name => <<"Crownm Public School">>, 'dgraph.type' => [<<"Institution">>]}
              },
    Schema =
        "name: string @index(exact) .\n		age: int .\n married: bool .\n friends: [uid] .\n loc: geo .\n type: string .\n coords: float .\n\n type Person {\n  name\n  age\n  married\n friends\n  loc\n   } \n type Institution {\n name\n  }",
    [
     ?_assertEqual(
        {ok, ?ALTER_OK, ?GRPC_OK}, api_dgraph_client:alter(#{schema => Schema}, Channel)
       )
    ],
    {ok, Match, _B} = api_dgraph_client:query(
                        #{mutations => [#{set_json => jsone:encode(Person)}], commit_now => true}, Channel
                       ),
    {ok, UidMap} = maps:find(uids, Match),
    AliceUid = map_get(<<"alice">>, UidMap),
    QueryUid =
        "query Me($id: string){\n	me(func: uid($id)) {\n  name\n   age\n  loc\n  raw_bytes\n     married  \n dgraph.type\n     friends @filter(eq(name, \"Bob\")) {\n name\n  age\n  dgraph.type  }\n  school {\n   name\n  dgraph.type\n   }\n  }\n }",
    [
     ?_assertMatch(
        {ok, _, ?GRPC_OK},
        api_dgraph_client:query(#{query => QueryUid, vars => #{"$id" => AliceUid}}, Channel)
       )
    ].



%% Bytes should ideally be string as their hex digits
txn_mutate_bytes(Channel) ->
    Schema = "name: string @index(exact) .\n bytes: [int] .\n\n type Person {\n  name\n  bytes\n   }",
    Query = #{ name => <<"Alice-new">>, 'dgraph.type' => <<"Person">>, bytes => binary_to_list(<<2,23,42,21,34,21,11,45>>)},
    QueryBytes = "{	q(func: eq(name, \"Alice-new\")) {\n  name\n  bytes\n   dgraph.type\n }\n  }",
    [
     ?_assertEqual(
        {ok, ?ALTER_OK, ?GRPC_OK}, api_dgraph_client:alter(#{schema => Schema}, Channel)
       ),
     ?_assertMatch(
        {ok, _, ?GRPC_OK}, api_dgraph_client:query(
                             #{mutations => [#{set_json => jsone:encode(Query)}], commit_now => true}, Channel )),
     ?_assertMatch(
        {ok, _, ?GRPC_OK},
        api_dgraph_client:query(#{query => QueryBytes}, Channel)
       )
    ].



txn_query_unmarshal(Channel) ->
    Schema = "name: string @index(exact) .\n age: int .\n married: bool .\n friends: [uid] .\n type Person { name age married Friends }  \n\n type Institution {\n name\n}",
    Person = #{uid => "_:bob", name => "Bob", age => 24, 'dgraph.type' => [<<"Person">>]},
    Query = "query Me($id: string){
			me(func: uid($id)) {
                          name
                          age
                          loc
                          raw_bytes
                          married
                          dgraph.type
                          friends @filter(eq(name, \"Bob\")) {
                                                            name
                                                            age
                                                            dgraph.type
                                                           }
                          school {
                                  name
                                  dgraph.type
                                 }
                         }
        }",
    PersonMutate = #{mutations => [#{set_json => jsone:encode(Person), commit_now => true}]},
    {ok, Match, ?GRPC_OK} = api_dgraph_client:query(PersonMutate, Channel),
    {ok, UidMap} = maps:find(uids, Match),
    BobUid = maps:values(UidMap),
    PersonAlice = #{
               uid => <<"_:alice">>,
               name => <<"Alice">>,
               age => 26,
               married => true,
               'dgraph.type' => [<<"Person">>],
               raw_bytes => [21, 32, 34, 11, 13],
               friends => [
                           #{uid => BobUid},
                           #{name => <<"Charlie">>, age => 29, 'dgraph.type' => [<<"Person">>]}
                          ],
               school => [#{name => <<"Crownm Public School">>, 'dgraph.type' => [<<"Institution">>]}]
                   },
    PersonMutate2 = #{mutations => [#{set_json => jsone:encode(PersonAlice), commit_now => true}], commit_now => true},
    MyValue =  api_dgraph_client:query(PersonMutate2, Channel),
    {ok, Match2, ?GRPC_OK} = MyValue,
    {ok, UidMap2} = maps:find(uids, Match2),
    AliceUid = map_get(<<"alice">>, UidMap2),
    [?_assertMatch({ok, _, ?GRPC_OK}, MyValue),
     ?_assertMatch(
       {ok, _, ?GRPC_OK},
       api_dgraph_client:query(
         #{
           query =>
               Query,
           vars => #{"$id" => AliceUid}
          },
         Channel
        )
      )].




%% Have helper function with all the schemas


init_client() ->
    ok = planetone_app:start_client_channel(),
    #{channel => channel1}.

term_client(_) ->
    planetone_app:stop([]).
